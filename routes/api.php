<?php

use Illuminate\Http\Request;
use DSPTCH\Events\MessageSent;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'FirestoreAuthController@login');

Route::post('test', function (Request $request) {

    echo file_get_contents('php://input');
});

Route::post('sendevent', function() {

    $jsondata = file_get_contents('php://input');
    $jsondata = json_decode($jsondata, TRUE);

    $event_type = $jsondata['event_type'];
    $location = $jsondata['message'];
    $channel_name = $jsondata['channel_name'];

    event(new MessageSent($event_type, $location, $channel_name));

    return 'sent';

});

Route::post('sendchatevent', function() {

    $jsondata = file_get_contents('php://input');
    $jsondata = json_decode($jsondata, TRUE);

    $user = $jsondata['user'];
    $message = $jsondata['message'];
    $channel_name = $jsondata['channel_name'];

    event(new MessageSent($user, $message, $channel_name));

    return 'sent';

});