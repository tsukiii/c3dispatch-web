<?php

namespace DSPTCH\Database;

use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Firestore\Transaction;
use Google\Cloud\Core\GeoPoint;

class FirestoreDB
{
    private $firestore;

    public function __construct () {
        $json_cred = base_path().'/c3chat-bbf9644ed2.json';

        $this-> firestore = new FirestoreClient ( [
            'keyFilePath' => $json_cred
        ]);

    }

    public function get_document(string $collection_name, Array $criteria) {

        $collection = $this -> firestore -> collection($collection_name);
        if (!empty($criteria)) {
            $query = $collection -> where($criteria[0], $criteria[1], $criteria[2]);
            return $query -> documents();
        }
        return $collection -> documents();
    
        // can be an array of documents
    }

    public function createDocument(string $collection, Array $contents) {

        $collection = $this -> firestore -> collection ($collection);
        $newUser = $collection->add([
            'user' => $contents['username'],
            'pass' => $contents['password']
        ]);

        return 'Registered!';
    }

    public function createRespondersDocument(string $collection, Array $contents) {

        $collection = $this -> firestore -> collection ($collection);
        $newUser = $collection->add([
            'location' => new GeoPoint(35, 5),
            'status' => $contents['status'],
            'userType' => $contents['userType'],
            'username' => $contents['username']
        ]);

        return 'Registered!';
    }


}