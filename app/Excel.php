<?php

namespace DSPTCH;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;
use \PhpOffice\PhpSpreadsheet\Reader\Xlsx as Reader;

class Excel {

    private $reader;
    private $spreadsheet;
    private $writer;
    private $currentcell;
    private $month;

    public function __construct() {
        $this -> reader = new Reader();
        $this -> spreadsheet = $this -> reader -> load(public_path()."\worksheet.xlsx");
        
    }

    /**
     * Sets the worksheet according to the month 
     * Please be reminded that due to the nature of the workbook, the months start from 
     * 1 - JANUARY, 2 - FEBRUARY, and so on... 
     * Be careful not to pass 0 as index!
     * 
     * @var int
     * @return void
     */
    public function setWorksheetMonth($month) {
        $this -> spreadsheet->setActiveSheetIndex($month);
        $this -> currentcell = $this -> getCurrentCell($month);
        $this -> month = $month;
    }

    /**
     * The meat of the details, pass it in one go,
     * then the function will dissect it automatically
     * this function also includes the writer, see example below
     * 
     * 
     * @var array
     * @return void
     */
    public function writeReport(Array $details) {

        /*
            a - date
            b - shift
            c - city/municipality
            d - place of incident
            e - incident monitored
            f - other services rendered
            g - casualty(injured)
            h - casualty(missing)
            i - casualty(dead)
            j - weather situation
            k - remarks

            $test = [
        
                'date' => 'test',
                'shift' => 'test',
                'city' => 'test',
                'incident_place' => 'test',
                'incident_monitored' => 'test',
                'other_services' => 'test',
                'injured' => 'test',
                'missing' => 'test',
                'dead' => 'test',
                'weather'  => 'test',
                'remarks' => 'test'
            ];
        */

        $this -> spreadsheet -> getActiveSheet()
            -> getCell('A'.$this -> currentcell)
            -> setValue($details['date']);

        $this -> spreadsheet -> getActiveSheet()
            -> getCell('B'.$this -> currentcell)
            -> setValue($details['shift']);

        $this -> spreadsheet -> getActiveSheet()
            -> getCell('C'.$this -> currentcell)
            -> setValue($details['city']);

        $this -> spreadsheet -> getActiveSheet()
            -> getCell('D'.$this -> currentcell)
            -> setValue($details['incident_place']);

        $this -> spreadsheet -> getActiveSheet()
            -> getCell('E'.$this -> currentcell)
            -> setValue($details['incident_monitored']);

        $this -> spreadsheet -> getActiveSheet()
            -> getCell('F'.$this -> currentcell)
            -> setValue($details['other_services']);

        $this -> spreadsheet -> getActiveSheet()
            -> getCell('G'.$this -> currentcell)
            -> setValue($details['injured']);

        $this -> spreadsheet -> getActiveSheet()
            -> getCell('H'.$this -> currentcell)
            -> setValue($details['missing']);

        $this -> spreadsheet -> getActiveSheet()
            -> getCell('I'.$this -> currentcell)
            -> setValue($details['dead']);

        $this -> spreadsheet -> getActiveSheet()
            -> getCell('J'.$this -> currentcell)
            -> setValue($details['weather']);

        $this -> spreadsheet -> getActiveSheet()
            -> getCell('K'.$this -> currentcell)
            -> setValue($details['remarks']);

        $this -> setLastCell($this -> currentcell, $this -> month);

        $writer = new Writer($this -> spreadsheet);
        $writer -> save(public_path().'\test1.xlsx');

        
    }

    

    /**
     * Gets the current cell which holds the current position of the last incident
     * Shouldn't bother with this
     * 
     * @return int
     */
    private function getCurrentCell($month) {

        $cellColumn = $this -> numericMonthtoCellEquivalent($month);
        $temp_reader = new Reader();
        $temp_spreadsheet  = $temp_reader -> load(public_path()."\worksheet.xlsx");
        
        return $temp_spreadsheet -> getSheet(12)
            -> getCell($cellColumn.'1')
            -> getValue();
    }

    /**
     * Sets the last cell to the next cell to prepare for next write
     * 
     * 
     * @var int, @var int
     * @return void
     */
    private function setLastCell($currentCell, $month) {

        $cellColumn = $this -> numericMonthtoCellEquivalent($month);
        $lastCell = $currentCell + 1;

        return $this-> spreadsheet -> getSheet(12)
            -> getCell($cellColumn.'1')
            -> setValue($lastCell);
    }

    /**
     * Simple mapper from month to cell equivalent
     * Used for the last sheet
     * 
     * @var int
     * @return int
     */
    private function numericMonthtoCellEquivalent($month) {
        switch ($month) {
            case 1:
                $cellColumn = 'A';
                break;
            case 2:
                $cellColumn = 'B';
                break;
            case 3:
                $cellColumn = 'C';
                break;
            case 4:
                $cellColumn = 'D';
                break;
            case 5:
                $cellColumn = 'E';
                break;
            case 6:
                $cellColumn = 'F';
                break;
            case 7:
                $cellColumn = 'G';
                break;
            case 8:
                $cellColumn = 'H';
                break;
            case 9:
                $cellColumn = 'I';
                break;
            case 10:
                $cellColumn = 'J';
                break;
            case 11:
                $cellColumn = 'K';
                break;
        }
        
        return $cellColumn;
    }
}