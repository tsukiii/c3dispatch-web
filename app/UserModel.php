<?php

namespace DSPTCH;

use DSPTCH\Database\FirestoreDB;

class UserModel
{

    private $name;
    private $lat_coord;
    private $lng_coord;
    private $event_id;
    private $status;

    private $db;

    public function __construct($user_id) {
        $this -> db = new FirestoreDB();

        $criteria = ['name', '=', $user_id];
        $collection = 'responders_location';
        $data = $this -> db -> get_document($collection, $criteria);

        if($data) {
            foreach($data as $details) {
                $this -> name = $details['name'];
                $this -> lat_coord = $details['lat'];
                $this -> lng_coord = $details['lng'];
                $this -> event_id = $details['event_id'];
                $this -> status = $details['status'];
            }
        }
    }

    public function get_name() {
        return $this -> name;
    }

    public function get_lat_coord() {
        return $this -> lat_coord;
    }

    public function get_lng_coord() {
        return $this -> lng_coord;
    }

    public function get_event_id() {
        return $this -> event_id;
    }

    public function get_status() {
        return $this -> status;
    }

    public function set_status($new_status) {
        $this -> status = $new_status;
    }

    public function assign_event_id($event_id) {
        $this -> event_id = $event_id;
    }




}