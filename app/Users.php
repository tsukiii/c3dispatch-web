<?php

namespace DSPTCH;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
    public $timestamps = false;
    protected $table = "users";
    protected $primaryKey = "id";
    protected $casts = ["id" => "INT"];

    protected $fillable = [
        'name', 'user_type', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
