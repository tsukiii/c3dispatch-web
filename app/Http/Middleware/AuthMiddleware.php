<?php

namespace DSPTCH\Http\Middleware;

use Closure;
use DSPTCH\TokenAuth;

class AuthMiddleware
{

        /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function __construct() {
        
    }
    public function handle($request, Closure $next) {

        if (!session()->exists('username')) {
            // user value cannot be found in session
            return redirect('/');
        }

        return $next($request);
    }

}
