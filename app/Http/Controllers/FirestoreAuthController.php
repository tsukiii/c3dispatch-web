<?php

namespace DSPTCH\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use DSPTCH\Database\FirestoreDB;
use Illuminate\Support\Facades\Hash;

class FirestoreAuthController extends Controller
{
    //
    public function login(Guard $auth_guard) {
        
        
        if($auth_guard -> validate()) {
            $user = $auth_guard -> user();
            $token = $auth_guard -> getToken();
            $uid = $user -> getAuthIdentifier();
            session() -> put ('token', $token);         
            session() -> put ('username', $uid);
            return view('welcome');
        }
        else {
            return view('pages.login');
        }  
    }
    
    public function register(Request $request) {        
        
        $firestore = new FirestoreDB();
        $firestore2 = new FirestoreDB();

        $username =  $request->input('username');
        $hashed_pw = Hash::make($request->input('password'));

        $credentials = ['username' => $username, 'password' => $hashed_pw];
        $firestore -> createDocument('user_table', $credentials);


        $user_type = $request->input('user_type');
        $data = ['status' => 'Available', 'userType' => $user_type, 'username' => $username];
        $firestore2 -> createRespondersDocument('Responders', $data);

        return view('welcome');
    }


    // public function register($username, $password) {
    //     $firestore = new FirestoreDB();

    //     $hashed_pw = Hash::make($password);

    //     $credentials = [$username, $hashed_pw];
    //     $firestore -> createDocument('user_table', $credentials);
    // }
}
