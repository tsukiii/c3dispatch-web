<?php

namespace DSPTCH\Http\Controllers;

use Illuminate\Http\Request;
use DSPTCH\Users;

class NavController extends Controller
{
    //
    public function GetResponders()
    {
        if ($user = Auth::user()){

            $responders = Users::all();
            return $responders;
        }else{
            return null;
        }
    }
}
