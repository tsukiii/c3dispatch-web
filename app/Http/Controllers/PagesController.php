<?php

namespace DSPTCH\Http\Controllers;

use Illuminate\Http\Request;
use DSPTCH\FirestoreUser;
use DSPTCH\Database\FirestoreDB;

class PagesController extends Controller
{

    private function getUsers() {
        $responders = new FirestoreDB();

        $responder_arr = $responders -> get_document('responders_location', []);
        $resp = [];
        foreach ($responder_arr as $responder) {
            $resp = [
                'event_id' => $responder['event_id'],
                'lat' => $responder['lat'],
                'lng' => $responder['lng'],
                'name' => $responder['name'],
                'status' => $responder['status'],
            ];
        }
        return $resp;
    }

    private function getUserDetails() {

        $firestore = new FirestoreDB();

        $user = $firestore -> get_document('Responders', ['username', '=', session() -> get('username')]);
        $user_details = [];

        foreach ($user as $details) {
            $user_details = [
                    'location' => $details['location'],
                    'status' => $details['status'],
                    'name' => $details['username']
                ];
        }

        return $user_details;
    }
    
    public function home(){
        $responders = $this->getUsers();
        $user_details = $this->getUserDetails();
        return view('home')->with('responders', $responders)->with('user_details', $user_details);
    }

    public function login(){
        $user_details = $this->getUserDetails();
        $responders = $this -> getUsers();
        return view('auth.login')->with('responders', $responders)->with('user_details', $user_details);
    }

    public function register(){
        $user_details = $this->getUserDetails();
        $responders = $this -> getUsers();
        return view('pages.register')->with('responders', $responders)->with('user_details', $user_details);
    }

    public function conversations(){
        $responders = $this->getUsers();
        $user_details = $this->getUserDetails();
        return view('pages.events')->with('responders', $responders)->with('user_details', $user_details);
    }

    public function dispatchers_location(){
        $user_details = $this->getUserDetails();
        $responders = $this -> getUsers();
        return view('pages.dispatcherslocation')->with('responders', $responders)->with('user_details', $user_details);
    }

    public function notifications(){
        $user_details = $this->getUserDetails();
        $responders = $this -> getUsers();
        return view('pages.notifications')->with('user_details', $user_details)->with('responders', $responders);
    }

    public function settings(){
        $user_details = $this->getUserDetails();
        $responders = $this -> getUsers();
        return view('pages.settings')->with('user_details', $user_details)->with('responders', $responders);
    }
}
