<?php

namespace DSPTCH\Http\Controllers;

use Illuminate\Http\Request;
use DSPTCH\Users;
class ConversationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responders = Users::all();
        $names = array('Danica', 'Archie', 'Aaron', 'Arvin', 'Bards');

        $messages = array(
            0 => array(
                'sender' => $names[1],
                'message' => 'Deserunt qwewq eu id ex veniam reprehenderit.'                
            ),
            1 => array(
                'sender' => $names[2],
                'message' => 'Ea labore fugiat adipisicing reprehenderit esse.'                
            ),
            2 => array(
                'sender' => $names[0],
                'message' => 'Amet sint sunt et adipisicing commodo.'                
            ),
            3 => array(
                'sender' => $names[3],
                'message' => 'Exercitation culpa adipisicing nostrud est nostrud eiusmod consectetur et dolore qui ad ex.'                
            ),
            4 => array(
                'sender' => $names[4],
                'message' => 'Adipisicing ex eu in in ex laborum.'                
            )                
        );

        return view('pages.events')->with('names', $names)->with('messages', $messages)->with('responders', $responders);
        // return $messages;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_message = $request->input('message_text');
        return $user_message;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
