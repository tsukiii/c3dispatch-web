<?php 

namespace DSPTCH;

use Storage;

class Drive 
{

    /**
     * Lists all the files in the root folder non-recursively
     * Meaning, it wont look on subdirectories
     * Returns metadata of each file in array format
     * 
     * @return array json format
     */
    public static function listAll() {
        $dir = '/';
        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
        //return $contents->where('type', '=', 'dir'); // directories
        return $contents->where('type', '=', 'file'); // files
    }

    /**
     * Lists all the files in the specified folder recursively
     * It uses the human readable folder name (not the path nor the basename)
     * It will look at subdirectories inside the folder
     * Returns metadata of each file in array format
     * Useful when you only want to retrieve files from a specific event
     * 
     * @return array json format
     */
    public static function listSpecificFolder($directoryName) {

        // This assumes the folder is located at the root node
        // Can be changed depending on the structure of the drive

        // Get root directory contents...
        $recursive = true;
        $contents = collect(Storage::cloud()->listContents('/', $recursive));
        // Find the folder you are looking for...
        $dir = $contents->where('type', '=', 'dir')
            ->where('filename', '=', $directoryName)
            ->first(); // There could be duplicate directory names!
        if ( ! $dir) {
            return 'No such folder!';
        }

        // Get the files inside the folder...
        $files = collect(Storage::cloud()->listContents($dir['path'], false))
            ->where('type', '=', 'file');

        return $files->mapWithKeys(function($file) {
            $filename = $file['filename'].'.'.$file['extension'];
            $path = $file['path'];
            // Use the path to download each file via a generated link..
            // Storage::cloud()->get($file['path']);
            return [$filename => $path];
        });
    }

    /**
     * Gets the specified file in a specific folder
     * filename parameter must have its extension type
     * 
     * @return File
     */
    public static function get($requestedFileName, $directoryName) {

        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($directoryName, $recursive));
        $file = $contents
            ->where('type', '=', 'file')
            ->where('filename', '=', pathinfo($requestedFileName, PATHINFO_FILENAME))
            ->where('extension', '=', pathinfo($requestedFileName, PATHINFO_EXTENSION))
            ->first(); // there can be duplicate file names!
        //return $file; // array with file info
        $rawData = Storage::cloud()->get($file['path']);
        return response($rawData, 200)
            ->header('ContentType', $file['mimetype'])
            ->header('Content-Disposition', "attachment; filename='$requestedFileName'");
    }

    /**
     * Creates a directory in the specified path
     * 
     * @return void
     */
    public static function createDir($directoryName) {
        Storage::cloud()->makeDirectory($directoryName);
        return 'Directory was created in Google Drive';
    }

    /**
     * Uploads a file to the drive.
     * filename with extension type should be provided 
     * localFilePath indicates the file path on the user's machine
     * directoryName indicates the path ON THE DRIVE.
     * 
     * @return void
     */
    public static function uploadFile($fileName, $fileData, $localFilePath, $directoryName) {

        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($directoryName, $recursive));
        $dir = $contents->where('type', '=', 'dir')
            ->where('filename', '=', $directoryName) // filename = directory name apparently, do not be confused!
            ->first(); // There could be duplicate directory names!
        if ( ! $dir) {
            return 'Directory does not exist!';
        }
        Storage::cloud()->put($dir['path'].'/'.$filename, $fileData);
        return 'File was saved to Google Drive';
    }

    /**
     * Deletes a file in a specific folder
     * Provide the filename complete with extension
     * 
     * @return void
     */
    public static function delete($fileName, $directoryName) {

        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($directoryName, $recursive));
        $file = $contents
            ->where('type', '=', 'file')
            ->where('filename', '=', pathinfo($fileName, PATHINFO_FILENAME))
            ->where('extension', '=', pathinfo($fileName, PATHINFO_EXTENSION))
            ->first(); // there can be duplicate file names!
        Storage::cloud()->delete($file['path']);
        return 'File was deleted from Google Drive';
    
    }

}