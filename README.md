# C3Dispatch - Web

C3Dispatch (also known as DSPTCH) is a proposed computer-aided dispatch system to the City Disaster Risk Reduction Management Office of the local government in the Philippines. The appplication is configured to be used by the City Government of Bi�an. DSPTCH was written by a team of interns from Malayan Colleges Laguna as part of their internship project. This web application is designed to be used in conjunction with [DSPTCH Mobile application](https://bitbucket.org/teamc5/c3dispatch-mobile/). 

## About
The website allows the operators in the headquarters to get an overview of the map, where they can see the current incidents (Fire, Medical, Vehicular). From the reports received at the headquarters, the user can create an incident report in the application, and can choose to deploy trained officers that on the field to deal with the issue. They will be notified through the mobile application. Real-time tracking of the dispatched officers is supported and is displayed on the map. Complete informatoin about the incident, including assessment reports are stored in the cloud using Google Firebase.

Summary or individual reported incidents can be exported into an Excel file with a single click, graphical charts of the incidents are also displayed on the homepage for further analysis.

## Dependencies
* [Laravel](https://laravel.com) - Back-end framework used
* [Google Firebase](https://firebase.google.com/) - NoSQL Database of choice
* [PHPSpreadsheet](https://github.com/PHPOffice/PhpSpreadsheet) - Creating excel files inside PHP
* [Pusher](https://pusher.com/) - Push notification system for both web and mobile application
* [Google Maps](https://maps.google.com) - For display of the map which includes the ongoing incidents and real-time position of the dispatched officers
* [JWT](https://github.com/lcobucci/jwt) - Authentication for both the web and mobile application
* [Bootstrap](https://getbootstrap.com/) - Styling the website

## Installation
This application requires [gRPC](https://grpc.io) to run Google API Library in PHP. Other than that, simply run
```
composer install
```

## Authors

* Archiebald Boy Alcain - [GitHub](https://github.com/tsukiii)
* Danica Nacionales 


## Notes
Unfortunately, this project is abandoned and therefore, is not maintained anymore. The app also broke when Google updated their [API usage policy](https://developers.google.com/places/web-service/usage-and-billing) on June 11, 2018.
