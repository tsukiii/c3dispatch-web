<?php

namespace DSPTCH\Http\Controllers;

use Illuminate\Support\Facades\DB;
use DSPTCH\Http\Controllers\Controller;

class UserController extends Controller
{
	public function index()
	{
		$users = DB::select('select * from table1');

		return view('index', ['users' => $users]);
	}
}