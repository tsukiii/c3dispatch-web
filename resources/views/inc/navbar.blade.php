<head>
    <style>
        
        .fire{background-image:url('{{ asset('img/fire.png') }}');}
        .vehicle_accident{background-image:url('{{ asset('img/vehicle_accident.png') }}');}
        .medical{background-image:url('{{ asset('img/medical.png') }}');}
    </style>
</head>

<script src="{{ asset('js/navbar.js') }}"></script>

<nav class="navbar navbar-inverse navbar-global navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">{{ config('app.name', 'DSPTCH') }}</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">            
          <ul class="nav navbar-nav navbar-user navbar-right">
                <!-- Authentication Links -->
                @if(session()->exists('username'))                  
                    <li><a href="#" data-toggle="modal" data-target="#myModal1" ><span class="glyphicon glyphicon-plus"></span> Add Event</a></li>
                    <li><a>| </a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> {{ $user_details["name"] }}</a></li>
                    <li><a href="/logout" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>

                    <form id="logout-form" action="/logout" method="GET" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @else
                    <li><a href="/login">Login</a></li>
                    <li><a href="/register">Register</a></li>
                @endif
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

@if(session()->exists('username'))   
    <nav class="navbar-primary">
    <a href="#" class="btn-expand-collapse"><span class="glyphicon glyphicon-menu-left"></span></a>
    
    <ul class="navbar-primary-menu">
        <li>
        <a href="/home"><span class="glyphicon glyphicon-list-alt"></span><span class="nav-label">Dashboard</span></a>
        <a href="/conversations"><span class="glyphicon glyphicon-envelope"></span><span class="nav-label">Events</span></a>
        
        <a href="/dispatcherslocation"><span class="glyphicon glyphicon-film"></span><span class="nav-label">Dispatchers Location</span></a>
        <a href="/notifications"><span class="glyphicon glyphicon-calendar"></span><span class="nav-label">Notifications</span></a>
        <a href="/settings"><span class="glyphicon glyphicon-cog"></span><span class="nav-label">Settings</span></a>
        
        </li>
    </ul>
    </nav>

    <!-- Modal -->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
    
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-body">
                <form>
                    <div class="cc-selector">
                        <h3>What type of event is occuring?</h3>
                        <div class="row">
                            <div class="col-sm-4">                            
                                    <center>
                                <input id="fire" type="radio" name="event-type" value="fire" onclick="showHint('event_type', 'fire')"/>
                                <label class="drinkcard-cc fire" for="fire"></label><br/>
                                Fire Incident</center>
                            </div>
                            <div class="col-sm-4">                                                    
                                    <center>
                                <input id="vehicle_accident" type="radio" name="event-type" value="vehicle_accident" onclick="showHint('event_type','vehicle_accident')"/>
                                <label class="drinkcard-cc vehicle_accident"for="vehicle_accident"></label>
                                Vehicle Accident</center>
                            </div>
                            <div class="col-sm-4">                            
                                    <center>
                                <input id="medical" type="radio" name="event-type" value="medical" onclick="showHint('event_type','medical')"/>
                                <label class="drinkcard-cc medical" for="medical"></label>           
                                Medical Emergency</center>
                            </div>
                        </div>                    
                    </div>
                </form>

                <div class="alert alert-danger collapse" id="event_alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>Alert!</strong> Please choose an event.
                </div>
            </div>
            <div class="modal-footer">
                <button id="type_button" type="button" class="btn btn-default btn-next">Next</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
            </div>
        </div>
    </div>
    
    
    <!-- Modal -->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
    
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-body">
                <h3>Where is the incident located?</h3>
                <div>
                    <div>
                        <div class="form-row"> 
                            <div class="form-group col-md-6">
                                Latitude:
                                <input id="lat2" type="text" class="form-control" placeholder="Latitude">
                            </div>
                            <div class="form-group col-md-6">
                                Longitude:
                                <input id="long2" type="text" class="form-control" placeholder="Longitude">
                            </div>
                        </div>
                        
                        Location:
                        <input type="text" class="form-control" id="location-text-box" />
                        <div id="map2" style="height: 55%; margin-top:1%"></div>
                        
                        <div id="infowindow-content">
                            <img src="" width="16" height="16" id="place-icon">
                            <span id="place-name"  class="title"></span><br>
                            <span id="place-address"></span>
                        </div>
                                                
                        <script async defer
                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBP5kXEpIl5cRZ7xOc65AIm1NWZt1-WtkQ&libraries=places">
                            // src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBP5kXEpIl5cRZ7xOc65AIm1NWZt1-WtkQ&libraries=places&callback=initMap">
                        </script>

                        <div class="alert alert-danger collapse" id="location_alert">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>Alert!</strong> Please choose a location.
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-next" onclick="showModal('myModal1')">Previous</button>
                <button id="location_button" type="button" class="btn btn-default btn-next">Next</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>  
            </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
    
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-body">
                <h3>Choose Responders</h3> 
                <div>
                    MAP - place map with responder's location here
                    
                </div>
                <hr/>
    
                <div id="responderslist_div" class="list-group">
                    {{--  this will contain the list of responders' names, distance, availability  --}}
                </div>            
                <div class="alert alert-danger collapse" id="responders_alert">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>Alert!</strong> Please choose responders.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-next" onclick="showModal('myModal2')">Previous</button>
                <button id="responders_button" type="button" class="btn btn-default btn-next">Next</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>  
            </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg">
        
                <!-- Modal content-->
                <div class="modal-content">
                <div class="modal-body">                    
                    <h3>Summary</h3>                    
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="inputEvent">Event Type</label>
                                <select id="inputEvent" class="form-control">                                    
                                    <option value="fire">Fire Incident</option>
                                    <option value="vehicle_accident">Vehicular Accident</option>
                                    <option value="medical">Medical Emergency</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">                            
                            <div class="form-group">
                                <label for="inputLocation">Location</label>
                                <input type="text" class="form-control" id="inputLocation" placeholder="1234 Main St">
                            </div>
                        </div>                                            
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label for="inputLatitude">Latitude</label>
                            <input type="text" class="form-control" id="inputLatitude">
                            </div>
                            <div class="form-group col-md-6">
                            <label for="inputLongitude">Longitude</label>
                            <input type="text" class="form-control" id="inputLongitude">
                            </div>
                        </div>
                        <div id="chosenResponders_div" class="list-group">                            
                        </div>
                    </form>
                    {{--  {!! Form::open(['action' => 'ConversationsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data',  ]) !!}
                        {{ Form::label('event_type_lbl', 'Event Type', ['class' => 'col-form-label']) }}    
                        {{ Form::select('event_type', ['fire' => 'Fire Emergency', 'vehicle_accident' => 'Vehicle Accident', 'medical_emergency' => 'Medical Emergency'], '. $event_summary.type .', ['class' => 'form-control']) }}   
                        <br/>
                        {{ Form::label('location_lbl', 'Location', ['class' => ' col-form-label']) }}    
                        MAP
                        
                    {!! Form::close() !!}  --}}
        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-next" onclick="showModal('myModal3')">Previous</button>
                    <button id="submitEvent_button" type="button" class="btn btn-primary btn-next" onclick="">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>  
                </div>
                </div>
            </div>
        </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        function showModal(id) {

            if(id == 'myModal4'){
                document.getElementById('myModal4').value = event_summary.type;
            }

            $(".modal").modal('hide');
            $(".modal-backdrop").remove();
            $("#" + id).modal();
        }    
    </script>

    
<script>
    var data = {
        lat: null,
        lng: null,
        name: null
    };

    var event_summary = {
        type: null,
        lat: null,
        long: null,
        landmark: null,
        responders: null,
        responders_id: null
    };

    window.onload = function(){

        ReturnResponders();
        initMap();
        document.getElementById('type_button').onclick = function(){
            if(event_summary.type != null){
                showModal('myModal2');
            }else{
                $("#event_alert").removeClass("in").show();
                $("#event_alert").delay(200).addClass("in").fadeOut(3000);
            }

            return false;
        }

        document.getElementById('location_button').onclick = function(){
            event_summary.lat = document.getElementById("lat2").value;
            event_summary.long = document.getElementById("long2").value;
            event_summary.landmark = document.getElementById("location-text-box").value;
            
            if(document.getElementById("lat2").value != '' && document.getElementById("long2").value != ''){
                PrintResponders();
                showModal('myModal3');
            }else{
                $("#location_alert").removeClass("in").show();
                $("#location_alert").delay(200).addClass("in").fadeOut(3000);
            }
        }

        document.getElementById('responders_button').onclick = function(){
            var checkboxes = document.getElementsByName('chkresponder');
            var checkboxesChecked = [];
            var resIds = [];
            // loop over them all
            for (var i=0; i<checkboxes.length; i++) {
                // And stick the checked ones onto an array...
                if (checkboxes[i].checked) {
                    var value = document.getElementById('cCheck' + (i)).innerHTML;                    
                    var resId = document.getElementById('cCheck' + (i)).getAttribute("name");

                    resIds.push(resId);
                    checkboxesChecked.push(value);
                }
            }
            
            event_summary.responders = checkboxesChecked;
            event_summary.responders_id = resIds;

            if(event_summary.responders.length > 0){
                console.log(event_summary);
                document.getElementById('inputEvent').value = event_summary.type;
                document.getElementById('inputLocation').value = event_summary.landmark;
                document.getElementById('inputLatitude').value = event_summary.lat;
                document.getElementById('inputLongitude').value = event_summary.long;

                PrintChosenResponders(resIds);

                showModal('myModal4');
            }else{
                $("#responders_alert").removeClass("in").show();
                $("#responders_alert").delay(200).addClass("in").fadeOut(3000);
            }
        }
        
        document.getElementById('submitEvent_button').onclick = function(){
            AddEvent(event_summary);
        }
    }
        
    function showHint(info_type, info){
        if (info.length == 0){

        }else{
            switch(info_type){
                case "event_type":
                    event_summary.type = info;
                    break;
                case "location":
                    if(event_summary.type != null){                                        
                        // event_summary.location = [document.getElementById("lat2").value, document.getElementById("long2").value];
                        break;
                    }
                case "responders":
                    var checkboxes = document.getElementsByName('chkresponder');
                    var checkboxesChecked = [];
                    // loop over them all
                    for (var i=0; i<checkboxes.length; i++) {
                        // And stick the checked ones onto an array...
                        if (checkboxes[i].checked) {
                            var value = document.getElementById('cCheck' + (i+1)).innerHTML;                            
                            checkboxesChecked.push(value);
                        }
                    }
                    event_summary.responders = checkboxesChecked;
                    alert(event_summary.responders);
                    break;
            }
        }
    }
</script>

@endif




{{--  
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-firestore.js"></script>
<script src="{{ asset('js/firestore.js') }}"></script>
<script src="{{ asset('js/map.js') }}"></script>  --}}