@extends('layouts.app')

@section('content')

<head>
    <style>
        .img_container {
            position: relative;
            width: 50%;
        }
         .user_icon{
             opacity: 1;
             display: block;
             width: 30%;
             height: auto;
             transition: .5s ease;
             backface-visibility: hidden;             
         }
    </style>
</head>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">

                    {!! Form::open(['action' => 'FirestoreAuthController@register', 'method' => 'GET', 'enctype' => 'multipart/form-data']) !!}
                        <div class="text-center">
                            <center>
                                <img src="{{ asset('img/default_user_icon.png') }}" class="user_icon rounded"><br/>
                                {{Form::submit('Choose Image', ['class'=>'btn btn-primary'])}}
                            </center>
                        </div>

                        <br/>
                        <div class="form-group">
                            <label for="username" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                {{Form::text('username', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="user_type" class="col-md-4 control-label">User Type</label>

                            <div class="col-md-6">
                                {{ Form::select('user_type', ['Command Center Officer' => 'Command Center Officer', 'Fire Responder' => 'Fire Responder', 'Medical Responder' => 'Medical Responder'], null, ['class' => 'form-control']) }}                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                {{ Form::password('password', ['class' => 'form-control']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
                        </div>

                        
                    {!! Form::close() !!}

                    {{--  <form class="form-horizontal" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="text-center">
                            <center>
                                <img src="{{ asset('img/default_user_icon.png') }}" class="user_icon rounded"><br/>
                                <button class="btn btn-primary">Upload Image </button>                                 
                            </center>
                        </div>
                        
                        <br/>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="user_type" class="col-md-4 control-label">User Type</label>

                            <div class="col-md-6">
                                <select id="user_type" name="user_type" class="form-control" id="exampleFormControlSelect1">
                                    <option>Command Center Officer</option>
                                    <option>Fire Responder</option>
                                    <option>Medical Responder</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>  --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
