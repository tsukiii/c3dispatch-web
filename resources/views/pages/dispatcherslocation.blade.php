@extends('layouts.app')

@section('content')

<script>
    var data = {
        lat: null,
        lng: null,
        name: null
    };
</script>

<div class="container col-lg-12" style="margin-top:2%">  

    <div class="contacts col-sm-2">  
        <div>
            <h4>Contacts</h4>        
            asd
        </div>
    </div>
    <div class="col-sm-10">
        <h4>Locations</h4>    
        Lat: 
        <input id="lat" name="lat" val="40.713956" />
        Long: 
        <input id="long" name="long" val="74.006653" />
        <input type="text" class="form-control" id="location-text-box1" />
        <div id="map1" style="height: 80%; margin-top:1%"></div>
    
        <div id="infowindow-content">
            <img src="" width="16" height="16" id="place-icon">
            <span id="place-name"  class="title"></span><br>
            <span id="place-address"></span>
        </div>

        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBP5kXEpIl5cRZ7xOc65AIm1NWZt1-WtkQ&libraries=places&callback=initMap">
        </script>
    </div>
</div>
@endsection


<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-firestore.js"></script>
<script src="{{ asset('js/firestore.js') }}"></script>
<script src="{{ asset('js/map.js') }}"></script>
