
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

  <head>    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta name="description" content="fresh Gray Bootstrap 3.0 Responsive Theme "/>
  <meta name="keywords" content="Template, Theme, web, html5, css3, Bootstrap,Bootstrap 3.0 Responsive Login" />
  <meta name="author" content="Adsays"/>
    <link rel="shortcut icon" href="favicon.png"> 
    
  <title>Home- Bootstrap 3.0.3 Responsive Login</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <!-- Demo CSS -->
    <link href="/css/demo.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/login-theme-1.css" rel="stylesheet" id="fordemo">

    
    <!-- <link href="assets/css/animate-custom.css" rel="stylesheet">  -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    
     <script src="/js/custom.modernizr.js" type="text/javascript" ></script>
   
  </head>
    <body class="fade-in">
      <!-- start Login box -->
      <div class="container" id="login-block">
        <div class="row justify-content-center">
          <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
             <div class="login-box clearfix animated flipInY">
                
                <div class="login-logo">
                  <a href="#"><img src="/img/login-logo.png" alt="Company Logo" /></a>
                </div> 
                <hr />

                <h1>Hello, {{ $name }} </h1>
                <button class="btn btn-login" onclick="location.href='{{ url('welcome') }}'">Login</button> 
                <div class="login-form">
              <form action="{{ route('user.route', $_GET['name']) }}" method="get">
                   <input type="text" placeholder="User name" class="input-field" name="name" id="name" required/> 
                   <input type="password"  placeholder="Password" class="input-field" required/> 
                   <button type="submit" class="btn btn-login">Login</button> 
              </form> 
              <div class="login-links"> 
                      <a href="forgot-password.html">
                         Forgot password?
                      </a>
                      <br />
                      <a href="sign-up.html">
                        Don't have an account? <strong>Sign Up</strong>
                      </a>
              </div>          
                </div>
                             
             </div>
              
          </div>
      </div>
      </div>
     
     
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')</script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="js/placeholder-shim.min.js"></script>        
        <script src="/js/custom.js"></script>
    </body>
</html>
